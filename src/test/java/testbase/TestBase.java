package testbase;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;

public class TestBase {
	
	public static ExtentReports extent;
	public static ExtentTest test;
	
	static {
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
		extent = new ExtentReports(System.getProperty("user.dir")+"//src//test//java//report//test" +formater.format(calendar.getTime())+".html",false);
		
	}
	
	public void getResult(ITestResult result) {
		
		if(result.getStatus() == ITestResult.SUCCESS) {
			test.log(LogStatus.PASS, result.getName() + "test is passed");
		}
		
		else if(result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, result.getName() + "test is skipped");
		}
		
		else if(result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, result.getName() + "test is failed");
		}
		
		else if(result.getStatus()==ITestResult.STARTED) {
			test.log(LogStatus.INFO, result.getName()+ "test has started");
		}
	}
	
  @Test
  public void f() {
  }
  @BeforeMethod
  public void beforeMethod(Method result) {
	  test = extent.startTest(result.getName());
	  test.log(LogStatus.INFO, result.getName()+ "test started");
  }

  @AfterMethod
  public void afterMethod(ITestResult result) {
	  getResult(result);
  }

  @AfterClass(alwaysRun = true)
  public void afterClass() {
	  
	  extent.endTest(test);
	  extent.flush();
  }

}
